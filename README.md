# Run KYPO Platform in Docker Containers within Vagrant

* tested on **Linux** and **Mac**

## Prerequisites (Vagrant)

Install the following technology:

Technology | URL to Download                           | Version
---------- | ---------------                           | -------
VirtualBox | https://www.virtualbox.org/wiki/Downloads | 5.2.34+
Vagrant    | https://www.vagrantup.com/downloads.html  | 2.0.2+
Vagrant Disk Plugin | https://github.com/sprotheroe/vagrant-disksize | 0.1.3+

## Prerequisites (Ansible)

Install the following technology:

Technology           | Installation                                             | Version
----------           | ---------------                                          | -------
Ansible              | `apt install ansible`                                    | 2.10.5+
community.docker     | `ansible-galaxy collection install community.docker`     | 1.5.0+
community.postgresql | `ansible-galaxy collection install community.postgresql` | 1.6.0
python3-passlib      | `apt install python3-passlib`                            | 1.7+
bcrypt               | `pip3 install bcrypt`                                    | 3.2+

## Run Vagrant VM

* **NOTE: 8GB RAM is required for the VM**


1. Configure access to the OpenStack cloud.

    1. [Obtain Application Credentials](https://docs.openstack.org/keystone/victoria/user/application_credentials.html).

    2. Edit an [local-demo-extra-vars.yml](local-demo-extra-vars.yml) file with the following variables using values obtained from the previous step.

        ```yaml
        # The URL of OpenStack Identity service API.
        kypo_crp_os_auth_url: <OS_AUTH_URL>
        # The ID of application credentials to authenticate at the OpenStack cloud platform.
        kypo_crp_os_application_credential_id: <OS_APPLICATION_CREDENTIAL_ID>
        # The secret string of `kypo_crp_os_application_credential_id`.
        kypo_crp_os_application_credential_secret: <OS_APPLICATION_CREDENTIAL_SECRET>
        ```

2. Configure access to the VM of the OpenStack cloud that has direct access to the virtual network dedicated to sandboxes (i.e. `kypo-proxy-jump`).

    1. Obtain access to the VM, i.e.:

        * hostname
        * username
        * passwordless SSH key

    2. Edit an [local-demo-extra-vars.yml](local-demo-extra-vars.yml) file with the following variables using values obtained from the previous step.

        ```yaml
        # The KYPO Jump host IP address or hostname.
        kypo_crp_proxy_host: <hostname>
        # The name of the user on the KYPO Jump host.
        kypo_crp_proxy_user: <username>
        ```

    3. Edit an [local-demo-secrets.yml](local-demo-secrets.yml) file with the following variable using values obtained from the previous step.

        ```yaml
        # The base64 encoded content of private SSL key used for communication with `kypo_crp_proxy_host`.
        kypo_crp_proxy_key: |-
            <passwordless-ssh-key
            spanning-multiple-lines>
        ```

    4. Insert the content of the public part of the key to `~/.ssh/authorized_keys` file of the user, specified in the previous step on the VM (i.e. `kypo-proxy-jump`).

3. Configure access to sandbox machines using Apache Guacamole remote desktop gateway.

    1. Configure and run [Apache Guacamole](https://github.com/boschkundendienst/guacamole-docker-compose) on the VM that has direct access to the virtual network dedicated to sandboxes (i.e. VM configured in the previous step).

    2. Create new dummy Guacamole user without no permissions.

    3. Edit an [local-demo-extra-vars.yml](local-demo-extra-vars.yml) file, uncomment and set the following variables.

        ```yaml
        # The URL of Apache Guacamole client with context path.
        kypo_crp_guacamole_url:
        # Name of the user without no permissions
        kypo_crp_guacamole_user:
        # Password for the `kypo_crp_guacamole_user`
        #kypo_crp_guacamole_user_password:
        ```

4. Configure DNS servers accessible from kypo-proxy-jump in [local-demo-extra-vars.yml](local-demo-extra-vars.yml)

    ```yaml
    # The list of IP addresses to custom DNS servers.
    kypo_crp_dns:
    ```

5. Create and connect to the virtual machine.

    Set the `EXTRA_VARS` environment variable as the comma-separated list of paths to the Ansible extra vars files, e.g. [local-demo-extra-vars.yml](local-demo-extra-vars.yml) and [local-demo-secrets.yml](local-demo-secrets.yml).

    ```shell
    $ vagrant box update
    $ EXTRA_VARS=./local-demo-extra-vars.yml,./local-demo-secrets.yml vagrant up
    ```

## Use KYPO

1. Access the local OIDC issuer page https://172.19.0.22:8443/csirtmu-dummy-issuer-server/ and accept the risks of using a self-signed certificate.

2. Authenticate with default user credentials and use the services running on https://172.19.0.22/.

    * **Admin users:** [kypo-admin]
    * **Regular users:** [kypo-user, john.doe, jane.doe]
    * **Password for all of them:** password

3. Add demo SB Definitions
    * **Linear**
      * URL: `git@git-internal-ssh:/repos/prototypes-and-examples/sandbox-definitions/kypo-crp-demo-training.git`
      * Revision: `v2.0.0`

    * **Adaptive**
      * URL: `git@git-internal-ssh:/repos/prototypes-and-examples/sandbox-definitions/kypo-crp-demo-training-adaptive.git`
      * Revision: `v1.0.1`

4. Upload demo Training Definitions that can be found in the `assets/training-definitions/` directory.
   * **Linear**: `training.json`.
   * **Adaptive**: `adaptive-training.json`.
