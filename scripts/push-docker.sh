#!/usr/bin/env bash

# These images are not yet pushed automatically via CI/CD

GITLAB_DOCKER_REGISTRY="registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository"

docker push "${GITLAB_DOCKER_REGISTRY}/kypo-local-oidc-issuer":master
docker push "${GITLAB_DOCKER_REGISTRY}/kypo-restfulgit":master
docker push "${GITLAB_DOCKER_REGISTRY}/kypo-ssh-git":master

echo "Push completed."
