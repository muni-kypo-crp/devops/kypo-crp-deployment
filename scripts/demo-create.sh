#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

"${DIR}"/elasticsearch-template.sh
"${DIR}"/add-users.sh
"${DIR}"/populate-git.sh

echo "--------------"
echo "Demo is ready."