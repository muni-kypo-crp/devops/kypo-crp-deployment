#!/usr/bin/env bash

# These images are not yet built automatically via CI/CD

GITLAB_DOCKER_REGISTRY="registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository"

# OIDC Issuer
docker build \
  -t "${GITLAB_DOCKER_REGISTRY}/kypo-local-oidc-issuer":master \
  git@gitlab.ics.muni.cz:kypo-crp/dependency-forks/csirtmu-oidc-overlay.git#master

# Git REST Proxy
docker build \
  -t "${GITLAB_DOCKER_REGISTRY}/kypo-restfulgit":master \
  git@gitlab.ics.muni.cz:kypo-crp/dependency-forks/csirtmu-docker-restfulgit.git#master

# SSH Git Repository Support
docker build \
  -t "${GITLAB_DOCKER_REGISTRY}/kypo-ssh-git":master \
  git@gitlab.ics.muni.cz:kypo-crp/dependency-forks/csirtmu-docker-ssh-git.git#master


