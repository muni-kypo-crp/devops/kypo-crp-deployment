# -*- mode: ruby -*-
# vi: set ft=ruby :

EXTRA_VARS=ENV['EXTRA_VARS'].to_s.strip.split(',')
raw_arguments=EXTRA_VARS.map{|file| "--extra-vars=@#{File.expand_path(file)}"}
DOCKER_ANSIBLE='provisioning/docker.yml'
OIDC_ANSIBLE='provisioning-oidc/oidc.yml'
GUACAMOLE_ANSIBLE='provisioning-guacamole/playbook-guacamole.yml'
PLAYBOOK_ANSIBLE='provisioning/playbook.yml'

OIDC_LOCAL_PROVIDER_EXTRA_VARS="oidc-local-provider.yml"
GUACAMOLE_EXTRA_VARS="guacamole-remote-desktop.yml"

def ansible_provision(config, playbook, raw_arguments)
  config.vm.provision :ansible do |provisioner|
    provisioner.compatibility_mode = "2.0"
    provisioner.playbook = playbook
    provisioner.galaxy_roles_path = 'provisioning/roles_required'
    provisioner.galaxy_role_file = "provisioning/requirements.yml"
    provisioner.raw_arguments = raw_arguments
  end
end

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/focal64"
  config.disksize.size = "40GB"

  config.vm.network "private_network", ip: "172.19.0.22"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = 8192
    vb.cpus = 4
  end

  ansible_provision(config, DOCKER_ANSIBLE, raw_arguments)
  ansible_provision(config, OIDC_ANSIBLE, raw_arguments)

  raw_arguments += ["--extra-vars=@#{OIDC_LOCAL_PROVIDER_EXTRA_VARS}"]
  ansible_provision(config, PLAYBOOK_ANSIBLE, raw_arguments)
end
