# Ansible role - KYPO Local OIDC

This role configures KYPO Local OIDC.

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

## Role parameters

Mandatory parameters

* `kypo_crp_host` - The FQDN or IP address of KYPO CRP.
* `kypo_crp_cert` - The base64 encoded content of SSL certificate that is used by KYPO CRP for HTTPS communication.
* `kypo_crp_cert_key` - The base64 encoded content of private SSL key of `kypo_crp_cert`.

Optional parameters.

* `kypo_crp_docker_network_name` - The name of the Docker network where the Internal Git will be connected (default: `kypo-platform-net`).
* `kypo_crp_users` - The list of KYPO CRP users that will be added to the local OIDC provider (default: `[]`).
    * `sub` - The unique identifier of the user within the OIDC provider.
    * `iss` - The URL of the OIDC provider.
    * `password` - A password of the user.
    * `email` - An email address of the user.
    * `fullName` - The user full name.
    * `givenName` - The user given name.
    * `familyName` - The user family name.
    * `admin` - The boolean value that represents whether the user is admin or not.

## Example

The simplest example.

```yml
roles:
    - role: kypo-crp-local-oidc
      kypo_crp_host: 172.19.0.22
      kypo_crp_cert: '{{ lookup("file", "/path/to/certificate.pem" }}'
      kypo_crp_cert_key: '{{ lookup("file", "/path/to/certificate.key" }}'
```

