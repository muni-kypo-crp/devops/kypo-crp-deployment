kypo_crp_templates_dir: '{{ role_path }}/templates'
kypo_crp_files_dir: '{{ role_path }}/files'
kypo_crp_cert_name: localhost.crt
kypo_crp_cert_key_name: localhost.key

kypo_crp_instance_name: default0
kypo_crp_host:
kypo_crp_url: https://{{ kypo_crp_host }}
kypo_crp_config_dest: /opt/kypo
kypo_crp_runtime_dest: '{{ kypo_crp_config_dest }}/runtime-data'
kypo_crp_os_auth_url:
kypo_crp_os_application_credential_id:
kypo_crp_os_application_credential_secret:
kypo_crp_proxy_host:
kypo_crp_proxy_user:
kypo_crp_proxy_key:
kypo_crp_dns: []

kypo_crp_docker_network_name: kypo-platform-net
kypo_crp_docker_network_mtu: 1442

kypo_crp_docker_services:
  user_and_group:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-uag-service
    image_tag: 'v1.1.87'
  sandbox_service:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-sandbox-service
    image_tag: 'v0.17.0'
  training:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-training-service
    image_tag: 'v1.2.0'
  adaptive_training:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-adaptive-training-service
    image_tag: 'v1.1.0'
  smart_assistant:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-adaptive-smart-assistant-service
    image_tag: 'v1.0.32'
  answers_storage:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-answers-storage-service
    image_tag: 'v1.0.9'
  training_feedback:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-training-feedback-service
    image_tag: 'v1.1.1'
  elasticsearch_service:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-elasticsearch-service
    image_tag: 'v1.0.35'
  frontend:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-frontend
    image_tag: 'v13.0.1'
  nginx:
    restart_policy: unless-stopped
    image: nginx
    image_tag: latest
  git_ssh:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-ssh-git
    image_tag: master
  git_rest:
    restart_policy: unless-stopped
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-restfulgit
    image_tag: master
  ansible_runner:
    image: registry.gitlab.ics.muni.cz:443/muni-kypo-crp/kypo-crp-artifact-repository/kypo-ansible-runner
    image_tag: 'v1.3.4'

elk_docker_services:
  elasticsearch:
    container_name: kypo-elasticsearch
    restart_policy: unless-stopped
    image: docker.elastic.co/elasticsearch/elasticsearch
    image_tag: 7.8.1
  logstash:
    container_name: kypo-logstash
    restart_policy: unless-stopped
    image: docker.elastic.co/logstash/logstash
    image_tag: 7.16.3

kypo_crp_oidc_post_logout_url: '{{ kypo_crp_url }}/logout-confirmed'
kypo_crp_oidc_silent_refresh_redirect_url: '{{ kypo_crp_url }}/silent-refresh.html'
kypo_crp_oidc_scopes:
  - openid
  - email
  - profile

kypo_crp_git: '{{ kypo_crp_git_internal }}'

kypo_crp_uag_service_container_name: uag-service
kypo_crp_uag_service_context_path: /kypo-rest-user-and-group/api/v1/
kypo_crp_uag_service_url: '{{ kypo_crp_url }}{{ kypo_crp_uag_service_context_path }}'
kypo_crp_uag_service_authorization_strategy_url: '{{ kypo_crp_uag_service_url }}users/info'
kypo_crp_uag_service_swagger_redirect_url: '{{ kypo_crp_uag_service_url }}webjars/springfox-swagger-ui/oauth2-redirect.html'
kypo_crp_uag_service_internal_url: 'http://{{ kypo_crp_uag_service_container_name }}:8084{{ kypo_crp_uag_service_context_path }}'
kypo_crp_uag_service_internal_registration_url: '{{ kypo_crp_uag_service_internal_url }}microservices'
kypo_crp_uag_service_internal_acquisition_url: '{{ kypo_crp_uag_service_internal_url }}users/info'

kypo_crp_sandbox_service_container_name: sandbox-service
kypo_crp_sandbox_service_context_path: /kypo-sandbox-service/api/v1/
kypo_crp_sandbox_service_url: '{{ kypo_crp_url }}{{ kypo_crp_sandbox_service_context_path }}'
kypo_crp_sandbox_service_internal_url: 'http://{{ kypo_crp_sandbox_service_container_name }}:8000{{ kypo_crp_sandbox_service_context_path }}'

kypo_crp_training_service_container_name: training-service
kypo_crp_training_service_context_path: /kypo-rest-training/api/v1/
kypo_crp_training_service_url: '{{ kypo_crp_url }}{{ kypo_crp_training_service_context_path }}'
kypo_crp_training_service_internal_url: 'http://{{ kypo_crp_training_service_container_name }}:8083{{ kypo_crp_training_service_context_path }}'
kypo_crp_training_service_swagger_redirect_url: '{{ kypo_crp_training_service_url }}webjars/springfox-swagger-ui/oauth2-redirect.html'

kypo_crp_adaptive_training_service_container_name: adaptive-training-service
kypo_crp_adaptive_training_service_context_path: /kypo-adaptive-training/api/v1/
kypo_crp_adaptive_training_service_url: '{{ kypo_crp_url }}{{ kypo_crp_adaptive_training_service_context_path }}'
kypo_crp_adaptive_training_service_internal_url: 'http://{{ kypo_crp_adaptive_training_service_container_name }}:8082{{ kypo_crp_adaptive_training_service_context_path }}'

kypo_crp_smart_assistant_service_container_name: smart-assistant
kypo_crp_smart_assistant_service_context_path: /kypo-adaptive-smart-assistant/api/v1/
kypo_crp_smart_assistant_service_url: '{{ kypo_crp_url }}{{ kypo_crp_smart_assistant_service_context_path }}'
kypo_crp_smart_assistant_service_internal_url: 'http://{{ kypo_crp_smart_assistant_service_container_name }}:8086{{ kypo_crp_smart_assistant_service_context_path }}'

kypo_crp_answers_storage_service_container_name: answers-storage
kypo_crp_answers_storage_service_context_path: /kypo-answers-storage/api/v1/
kypo_crp_answers_storage_service_url: '{{ kypo_crp_url }}{{ kypo_crp_answers_storage_service_context_path }}'
kypo_crp_answers_storage_service_internal_url: 'http://{{ kypo_crp_answers_storage_service_container_name }}:8087{{ kypo_crp_answers_storage_service_context_path }}'

kypo_crp_training_feedback_service_container_name: training-feedback
kypo_crp_training_feedback_service_context_path: /kypo-training-feedback/api/v1/
kypo_crp_training_feedback_service_url: '{{ kypo_crp_url }}{{ kypo_crp_training_feedback_service_context_path }}'
kypo_crp_training_feedback_service_internal_url: 'http://{{ kypo_crp_training_feedback_service_container_name }}:8088{{ kypo_crp_training_feedback_service_context_path }}'

kypo_crp_angular_frontend_container_name: angular-frontend
kypo_crp_angular_frontend_internal_url: 'http://{{ kypo_crp_angular_frontend_container_name }}:8000'

kypo_crp_elasticsearch_service_container_name: elasticsearch-service
kypo_crp_elasticsearch_service_context_path: /kypo-elasticsearch-service/api/v1/
kypo_crp_elasticsearch_service_url: '{{ kypo_crp_url }}{{ kypo_crp_elasticsearch_service_context_path }}'
kypo_crp_elasticsearch_service_internal_url: 'http://{{ kypo_crp_elasticsearch_service_container_name }}:8085{{ kypo_crp_elasticsearch_service_context_path }}'

kypo_crp_users: []
