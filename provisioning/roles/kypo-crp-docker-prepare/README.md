# Ansible role - KYPO CRP Docker preparation

This role prepares Docker for KYPO CRP.

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

## Role parameters

Mandatory parameters

* `kypo_crp_docker_network_name` - The name of the Docker network that will be created (default: `kypo-platform-net`).
* `kypo_crp_docker_mtu` - The maximum transmission unit for KYPO services (default: `1442`).
* `kypo_crp_docker_images` - The list of KYPO CRP services Docker image names (default: [see](defaults/main.yml)).

## Example

The simplest example.

```yaml
roles:
    - role: kypo-crp-docker-prepare
      kypo_crp_docker_network_name: kypo-platform-net
      kypo_crp_docker_network_mtu: 1442
      kypo_crp_docker_images:
        - munikypo/crp-demo-uag-service
        - ...
```

