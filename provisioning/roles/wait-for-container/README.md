# Ansible role - Wait For Container

This role waits till Docker container opens ports on specified network.

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

## Role parameters

Mandatory parameters

* `wait_for_container_name` - The name of the Docker container.
* `wait_for_container_network` - The name of the Docker network to which the `wait_for_container_name` is connected.

## Example

The simplest example.

```yml
roles:
    - role: wait-for-container
      wait_for_container_name: container-name
      wait_for_container_network: docker-network-name-where-container-is-connected
```

