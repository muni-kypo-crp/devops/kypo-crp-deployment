INSERT INTO idm_group(name, external_id, status, description, expiration_date) 
VALUES ('All Mighty Users', NULL, 'VALID', 'All mighty group', NULL) ON CONFLICT DO NOTHING;

INSERT INTO idm_group(name, external_id, status, description, expiration_date) 
VALUES ('Instructor', NULL, 'VALID', 'Adaptive and Linear Training Designer and Organizer', NULL) ON CONFLICT DO NOTHING;

INSERT INTO idm_group_role(role_id, idm_group_id) 
VALUES ((SELECT id FROM role WHERE role_type='ROLE_USER_AND_GROUP_ADMINISTRATOR'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;

INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_TRAINING_ADMINISTRATOR'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_TRAINING_ORGANIZER'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_TRAINING_DESIGNER'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_TRAINING_TRAINEE'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;

INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_ADAPTIVE_TRAINING_ADMINISTRATOR'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_ADAPTIVE_TRAINING_ORGANIZER'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_ADAPTIVE_TRAINING_DESIGNER'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_ADAPTIVE_TRAINING_TRAINEE'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;

INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_KYPO-SANDBOX-SERVICE_ADMIN'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_KYPO-SANDBOX-SERVICE_ORGANIZER'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_KYPO-SANDBOX-SERVICE_DESIGNER'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_KYPO-SANDBOX-SERVICE_TRAINEE'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;

INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_TRAINING_ORGANIZER'), (SELECT id FROM idm_group WHERE name='Instructor')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_TRAINING_DESIGNER'), (SELECT id FROM idm_group WHERE name='Instructor')) ON CONFLICT DO NOTHING;

INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_ADAPTIVE_TRAINING_ORGANIZER'), (SELECT id FROM idm_group WHERE name='Instructor')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_ADAPTIVE_TRAINING_DESIGNER'), (SELECT id FROM idm_group WHERE name='Instructor')) ON CONFLICT DO NOTHING;

INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_KYPO-SANDBOX-SERVICE_ORGANIZER'), (SELECT id FROM idm_group WHERE name='Instructor')) ON CONFLICT DO NOTHING;
INSERT INTO idm_group_role(role_id, idm_group_id)
VALUES ((SELECT id FROM role WHERE role_type='ROLE_KYPO-SANDBOX-SERVICE_DESIGNER'), (SELECT id FROM idm_group WHERE name='Instructor')) ON CONFLICT DO NOTHING;

{% for kypo_crp_user in kypo_crp_users %}
{% if kypo_crp_user.admin %}
INSERT INTO user_idm_group(user_id, idm_group_id)
VALUES ((SELECT id FROM users WHERE sub='{{ kypo_crp_user.sub }}'), (SELECT id FROM idm_group WHERE name='All Mighty Users')) ON CONFLICT DO NOTHING;
{% else %}
INSERT INTO user_idm_group(user_id, idm_group_id)
VALUES ((SELECT id FROM users WHERE sub='{{ kypo_crp_user.sub }}'), (SELECT id FROM idm_group WHERE name='DEFAULT-GROUP')) ON CONFLICT DO NOTHING;
{% endif %}
{% endfor %}
