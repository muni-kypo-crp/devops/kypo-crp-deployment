#!/usr/bin/env bash

docker exec -i kypo-postgres bash -c 'su postgres -c "psql --dbname user-and-group -a"' < assets/data-import/user-and-groups-init.sql
