# Ansible role - KYPO elk

This role deploys Elasticsearch, Logstash and optionally Kibana behind Nginx HTTPS reverse proxy for KYPO.

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

* At least 1GB RAM on target machine.

## Role parameters

You can override default values of these parameters:

* `kypo_crp_config_dest` - Path, where all configuration will be created.

For more information on how to configure the ELK stack, see the generic Ansible role [elk](https://gitlab.ics.muni.cz/muni-kypo/ansible-roles/elk) that this one uses.

## Example

The simplest example.

```yml
roles:
    - role: kypo-crp-elk
```
